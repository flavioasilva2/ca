# CA
Criando os arquivos do CA:

Chave privada:

```
openssl genrsa -out ca.key 4096
```

CSR:
```
openssl req -new -key ca.key -config ca_csr.cnf -out ca.csr -nodes
```

Certificado auto-assinado do CA:
```
openssl x509 -req -in ca.csr -signkey ca.key -days 3650 -extfile ca_csr.extensions.cnf -out ca.crt
```

Crie o arquivo de serial:
```
echo '01' > serial
```

Crie a base:
```
touch index.txt
```

Crie o diretório dos novos certificados:
```
mkdir newCerts
```

# Certificados

Defina o nome base dos novos arquivos:
```
export BASE="..."
```

Copie os templates do novo CSR para a pasta newCerts:

```
cp example_req.cnf newCerts/$BASE.cnf
cp example_req.extensions.cnf newCerts/$BASE.extensions.cnf
```

Agora edite esses dois arquivos para ficar de acordo com o desejado.

Crie a chave privada do novo certificado:
```
openssl genrsa -out newCerts/$BASE.key 4096
```

Crie o CSR para o novo certificado:
```
openssl req -new -key newCerts/$BASE.key -out newCerts/$BASE.csr -config newCerts/$BASE.cnf
```

Assine o novo certificado:
```
openssl ca -config ca.conf -out newCerts/$BASE.crt -extfile newCerts/$BASE.extensions.cnf -in newCerts/$BASE.csr
```

Gere a cadeia completa de certificados:
```
openssl x509 -in newCerts/$BASE.crt > newCerts/$BASE_tmp.crt && rm -f newCerts/$BASE.crt && mv newCerts/$BASE_tmp.crt newCerts/$BASE.crt
cat newCerts/$BASE.crt ca.crt > newCerts/$BASE.fullchain.crt
```

# Parametros Diffie-Hellman

Algumas aplicações requerem parametros Diffie-Hellman, para gera-los use:

```
openssl dhparam -out newCerts/$BASE.dh.pem 2048
```
